from sqlalchemy import Column, Integer, Float, Date, ForeignKey
from sqlalchemy.orm import relationship

from app.db import Base


class Salary(Base):
    __tablename__ = 'salary'

    id = Column(Integer, primary_key=True)
    amount = Column(Float, nullable=False)
    promotion_date = Column(Date, nullable=False)

    user_id = Column(ForeignKey("user.id"), unique=True, nullable=False, index=True)
    user = relationship('User', back_populates="salary")