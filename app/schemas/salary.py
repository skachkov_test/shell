from pydantic import BaseModel

from datetime import date


class Salary(BaseModel):
    amount: float
    promotion_date: date
    
    class Config:
        orm_mode = True

class ResultSalary(BaseModel):
    username: str
    salary: Salary

    class Config:
        orm_mode = True