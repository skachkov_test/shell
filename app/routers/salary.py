from fastapi import APIRouter, Security

from app.utils.permissions import get_current_user
from app.utils.auth import Auth
from app.schemas.salary import ResultSalary
from app.models.user import User


auth = Auth()


router = APIRouter(
    tags=['Salary'],
    prefix='/salary',
)

@router.get("", response_model=ResultSalary)
def get_salary(user: User = Security(get_current_user)):
    return user