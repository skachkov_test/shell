from fastapi.testclient import TestClient

from app.main import app, test_data


client = TestClient(app)


def test_get_salary():
    token = client.post(
        "/auth/signin",
        json={
            "username": test_data["test_user"][0].username, 
            "password": "test1"
        },
    ).json()
    response = client.get(
        "/salary",
        headers={
            "Authorization": f"Bearer { token['token'] }"
        }
    )
    assert response.status_code == 200
    assert response.json() == {
            "username": test_data["test_user"][0].username,
            "salary": {
                "amount": test_data["test_salary"][0].amount,
                "promotion_date": str(test_data["test_salary"][0].promotion_date)
            }
        }


def test_get_salary_without_token():
    response = client.get(
        "/salary"
    )
    assert response.status_code == 403
    assert response.json() == {
            "detail": "Not authenticated"
        }
